require "types"

ShipType = {}
ShipType.__index = ShipType

function ShipType.new(image, imageRotation, acceleration, turningSpeed)
    local t = {
        image          = image,
        imageRotation  = imageRotation,
        acceleration   = 10,
        turningSpeed   = 2,
    }

    return setmetatable(t, ShipType)
end

return ShipType
