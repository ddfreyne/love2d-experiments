require "types"

Ship = {}
Ship.__index = Ship

function Ship.new(x, y, shipType)
    local t = {
        shipType       = shipType,
        rect           = Rect.new(x, y, shipType.image:getWidth(), shipType.image:getHeight()),
        angle          = 0,
        velocity       = Vector.new(0, 0),
        thrustersOn    = false,
        isTurningLeft  = false,
        isTurningRight = false,
    }

    return setmetatable(t, Ship)
end

function Ship:update(delta)
	c = math.cos(self.angle)
    s = math.sin(self.angle)

	if self.thrustersOn then
		self.velocity.x = self.velocity.x + c * delta * self.shipType.acceleration
		self.velocity.y = self.velocity.y + s * delta * self.shipType.acceleration
	end

    if self.isTurningLeft then
        self.angle = self.angle - self.shipType.turningSpeed * delta
    end

    if self.isTurningRight then
        self.angle = self.angle + self.shipType.turningSpeed * delta
    end

    self.rect.origin.x = self.rect.origin.x + self.velocity.x
    self.rect.origin.y = self.rect.origin.y + self.velocity.y
end

function Ship:render()
	love.graphics.push()

	love.graphics.translate(self.rect:xMiddle(), self.rect:yMiddle())
	love.graphics.rotate(self.angle + self.shipType.imageRotation)

    love.graphics.draw(
    	self.shipType.image,
    	-self.rect.size:xMiddle(),
    	-self.rect.size:yMiddle())

    love.graphics.pop()
end

return Ship
