require "types"

ShipType = require("entities/ship_type")
Ship     = require("entities/ship")

local world = {}
local shader
local canvas
local t = 0

function love.load()
    local imageName = "ship.png"
    local normalMapName = "ship-normal.png"

    -- local imageName = "face.jpg"
    -- local normalMapName = "face_norm.jpg"

    local image = love.graphics.newImage("assets/" .. imageName)
    normalMap = love.graphics.newImage("assets/" .. normalMapName)
    local shipType = ShipType.new(image, 0, 10, 2)
    world.ship = Ship.new(20, 40, shipType)

    shader = love.graphics.newShader("shaders/normal.fs")
end

function love.update(delta)
    t = t + delta
    local x, y = love.mouse.getPosition()
    local w, h = love.window.getWidth(), love.window.getHeight()

    shader:send("u_normals", normalMap)
    shader:send("inLightPos", { x / w, 1.0 - y / h })
    shader:send("resolution", { w, h })

    world.ship.thrustersOn    = love.keyboard.isDown("up")
    world.ship.isTurningLeft  = love.keyboard.isDown("left")
    world.ship.isTurningRight = love.keyboard.isDown("right")

    world.ship:update(delta)
end

function love.draw()
    love.graphics.setShader(shader)
    world.ship:render()
    love.graphics.setShader()
end
