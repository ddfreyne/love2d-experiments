uniform number time;
uniform sampler2D u_normals;
uniform vec2 inLightPos;
uniform vec2 resolution;

#define PI 3.141592658

const vec4 LightColor = vec4(1.0, 1.0, 1.0, 1.0);
const vec4 AmbientColor = vec4(1.0, 1.0, 1.0, 0.5);
const vec3 Falloff = vec3(.9, .9, 0.0);

vec4 effect(vec4 vColor, sampler2D u_texture, vec2 vTexCoord, vec2 vScreenCoord)
{
	vec3 LightPos = vec3(inLightPos, 0.1);

    //RGBA of our diffuse color
    vec4 DiffuseColor = texture2D(u_texture, vTexCoord);

    //RGB of our normal map
    vec3 NormalMap = texture2D(u_normals, vTexCoord).rgb;
    // NormalMap.g = 1.0 - NormalMap.g;

    //The delta position of light
    vec3 LightDir = vec3(LightPos.xy - (vScreenCoord.xy / resolution.xy), LightPos.z);

    //Correct for aspect ratio
    LightDir.x *= resolution.x / resolution.y;

    //Determine distance (used for attenuation) BEFORE we normalize our LightDir
    float D = length(LightDir);

    //normalize our vectors
    vec3 N = normalize(NormalMap * 2.0 - 1.0);
    vec3 L = normalize(LightDir);

    //Pre-multiply light color with intensity
    //Then perform "N dot L" to determine our diffuse term
    vec3 Diffuse = (LightColor.rgb * LightColor.a) * max(dot(N, L), 0.0);

    //pre-multiply ambient color with intensity
    vec3 Ambient = AmbientColor.rgb * AmbientColor.a;

    //calculate attenuation
    float Attenuation = 1.0 / ( Falloff.x + (Falloff.y*D) + (Falloff.z*D*D) );

    //the calculation which brings it all together
    vec3 Intensity = Ambient + Diffuse * Attenuation;
    vec3 FinalColor = DiffuseColor.rgb * Intensity;

    vec4 res = vColor * vec4(FinalColor, DiffuseColor.a);

    return res;
}
